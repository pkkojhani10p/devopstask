FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app
EXPOSE 80
EXPOSE 443
RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . .
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs
COPY --from=build-env /app/out .
# Run the app on container startup
# Use your project name for the second parameter
# e.g. MyProject.dll

#ENTRYPOINT [ "dotnet", "ReactWithDotNet.dll" ]
CMD ASPNETCORE_URLS=http://*:$PORT dotnet ReactWithDotNet.dll